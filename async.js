// fetch domyślnie to metoda GET, spodziewa się JSON , więc trzeba mu zwrócić JSON, z serwera zazwyczaj pobieramy 
// textowe. JSON - format do operowania na AJAXIE albo obiekt w JS , który odpowiada za zamienianie na format JSON 
//pars zamienia tekst na format JSON

function prepareList (response) {
    const list = document.getElementById('todoList');
    response.forEach(todo => {
        
        const li = document.createElement('li');
        list.appendChild(li);
        const span = document.createElement('span');
        span.innerText = todo.title;
        li.setAttribute('data-id', todo.id);
    
        const deleteButton = document.createElement('button');
        deleteButton.setAttribute('class' , 'deleteBtn'); 
        deleteButton.innerText = 'Usuń';
        const editButton = document.createElement('button');
        editButton.setAttribute('class' , 'editBtn');
        editButton.innerText = 'Edytuj';
        const doneButton = document.createElement('button');
        doneButton.setAttribute('class' , 'doneBtn');
        doneButton.innerText = 'Zrobione';

        li.appendChild(span);
        li.appendChild(doneButton);
        li.appendChild(editButton);
        li.appendChild(deleteButton);
    });
}


export function getTodoList() {
    document.getElementById('todoList').innerHTML = ''; 
    fetch('http://195.181.210.249:3000/todo/').then((response) => {
        return response.json();
    }).then((response) => {
        prepareList(response); // funkcja napisana wyżej 
        return response;
    }).catch (() => {
        console.log('Error');
    });
}

export function postElement (title) {
    fetch('http://195.181.210.249:3000/todo/', {
        headers: {
            'Content-Type' : 'application/json',
        },
        method: 'POST',
        body: JSON.stringify({title: title }),
    }).then(() => getTodoList());
}


export function deleteElement(id) {
    fetch('http://195.181.210.249:3000/todo/' + id, {
        headers: {
            'Content-Type': 'application/json',
        },
        method: 'DELETE',
    }).then(() => {
        getTodoList();
    });
}

export function putElement(title,id) {
    fetch('http://195.181.210.249:3000/todo/' + id, {
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'PUT',
      body: JSON.stringify({ title: title }),
    }).then(() => getTodoList());
  }
  









