# TodoList

This is my first ToDoLista. The application allows you to add a new item, delete an item, mark items that have been made, and edit existing items and uses server queries and was written using HTML/CSS/JS as part of a bootcamp at the Future Collars.