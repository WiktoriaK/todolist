import { getTodoList, postElement, deleteElement, putElement } from "./async.js";

let myList;
let myModal;
let closePopup;
let cancelTodo;
let currentId;
let popupInput;
let acceptTodo;

// funkcja tworzy nowy obiekt i dodaje do tablicy , ale w kodzie, użytkownik go nie widzi
function createListItem() {
  //pobieram dane od użytkownika 
  let inputValue = document.getElementById('todoInput').value; 
  if (inputValue.trim() !== '') {
    postElement(inputValue);
    document.getElementById('todoInput').value = '';
  }
}
function openModal(title) {
  document.querySelector('.modal-content').classList.add('modal-show');
  popupInput.value = title;
}

function closeModal() {
  document.querySelector('.modal-content').classList.remove('modal-show');
  popupInput.value = '';
}

function editTodo() {
  const value = popupInput.value;
  if (value.trim() !== '') {
    putElement(value, currentId);
    closeModal();
  }
}

function addEvents() {
  myModal = document.querySelector('.modal');
  closePopup = document.querySelector('#closePopup');
  cancelTodo = document.querySelector('#cancelTodo');
  popupInput = document.querySelector('#popupInput');
  acceptTodo = document.querySelector('#acceptTodo');
  //dodanie funkcji do głównego przycisku  (addNewTodoBtn)

  myList = document.getElementById('todoList'); 
  const addButton = document.getElementById('addNewTodoBtn');
  addButton.addEventListener('click' , createListItem);
  closePopup.addEventListener('click', closeModal);
  cancelTodo.addEventListener('click', closeModal);
  acceptTodo.addEventListener('click', editTodo);

  //dodanie funkcji do pozostałych przycisków 
  myList.addEventListener('click', function(event) {
    
    if (event.target.className === 'deleteBtn') { //usuwanie 
      const todoId = event.target.parentElement.dataset.id;
      deleteElement(todoId);
      event.target.parentElement.remove();
     
    }
    else if (event.target.className === 'editBtn') {
      const li = event.target.parentElement;
      currentId = li.dataset.id;
      openModal(li.querySelector('span').innerText);
    }
    else if (event.target.className === 'doneBtn') {
      event.target.parentElement.style.textDecoration = 'line-through';
    }
  })
}

getTodoList();
addEvents();
